/* $Id: $ */

/* host/net/net-tap.c - TUN/TAP Ethernet support: */

/*
 * Copyright (c) 2012 Thomas Bogendörfer
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by Matt Fredette.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <tme/common.h>
_TME_RCSID("$Id: $");

/* includes: */
#include <tme/generic/ethernet.h>
#include <tme/threads.h>
#include <tme/misc.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <net/if.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#if defined(HAVE_SYS_SOCKIO_H)
#include <sys/sockio.h>
#elif defined(HAVE_SYS_SOCKETIO_H)
#include <sys/socketio.h> 
#endif /* HAVE_SYS_SOCKETIO_H */
#include <sys/ioctl.h>
#ifdef HAVE_IOCTLS_H
#include <ioctls.h>
#endif /* HAVE_IOCTLS_H */
#ifdef HAVE_NET_IF_ETHER_H
#include <net/if_ether.h>
#endif /* HAVE_NET_IF_ETHER_H */
#ifdef HAVE_NET_ETHERNET_H
#include <net/ethernet.h>
#endif /* HAVE_NET_ETHERNET_H */
#include <netinet/ip.h>
#ifdef HAVE_NET_IF_DL_H
#include <net/if_dl.h>
#endif /* HAVE_NET_IF_DL_H */
#include <arpa/inet.h>

/* FIXME: Linux specific part, needs to be adapted */
#include <net/if.h>
#include <linux/if_tun.h>

/* macros: */

/* ARP and RARP opcodes: */
#define TME_NET_ARP_OPCODE_REQUEST	(0x0001)
#define TME_NET_ARP_OPCODE_REPLY	(0x0002)
#define TME_NET_ARP_OPCODE_REV_REQUEST	(0x0003)
#define TME_NET_ARP_OPCODE_REV_REPLY	(0x0004)

/* the callout flags: */
#define TME_NET_TAP_CALLOUT_CHECK	(0)
#define TME_NET_TAP_CALLOUT_RUNNING	TME_BIT(0)
#define TME_NET_TAP_CALLOUTS_MASK	(-2)
#define  TME_NET_TAP_CALLOUT_CTRL	TME_BIT(1)
#define  TME_NET_TAP_CALLOUT_READ	TME_BIT(2)

/* structures: */

/* our internal data structure: */
struct tme_net_tap {

  /* backpointer to our element: */
  struct tme_element *tme_net_tap_element;

  /* our mutex: */
  tme_mutex_t tme_net_tap_mutex;

  /* our reader condition: */
  tme_cond_t tme_net_tap_cond_reader;

  /* the callout flags: */
  unsigned int tme_net_tap_callout_flags;

  /* our Ethernet connection: */
  struct tme_ethernet_connection *tme_net_tap_eth_connection;

  /* the BPF file descriptor: */
  int tme_net_tap_fd;

  /* the size of the packet buffer for the interface: */
  size_t tme_net_tap_buffer_size;

  /* the packet buffer for the interface: */
  tme_uint8_t *tme_net_tap_buffer;

  /* the next offset within the packet buffer, and the end of the data
     in the packet buffer: */
  size_t tme_net_tap_buffer_offset;
  size_t tme_net_tap_buffer_end;

  /* timestamp for buffer receive */
  struct timeval tme_net_tap_rx_tstamp;

  /* when nonzero, the packet delay time, in microseconds: */
  unsigned long tme_net_tap_delay_time;

  /* all packets received on or before this time can be released: */
  struct timeval tme_net_tap_delay_release;

  /* when nonzero, the packet delay sleep time, in microseconds: */
  unsigned long tme_net_tap_delay_sleep;

  /* when nonzero, the packet delay is sleeping: */
  int tme_net_tap_delay_sleeping;
};

static int
_tme_net_tap_alloc(char *dev, int flags) {

  struct ifreq ifr;
  int fd, err;
  char *clonedev = "/dev/net/tun";

  /* Arguments taken by the function:
   *
   * char *dev: the name of an interface (or '\0'). MUST have enough
   *   space to hold the interface name if '\0' is passed
   * int flags: interface flags (eg, IFF_TUN etc.)
   */

   /* open the clone device */
   if( (fd = open(clonedev, O_RDWR)) < 0 ) {
     return fd;
   }

   /* preparation of the struct ifr, of type "struct ifreq" */
   memset(&ifr, 0, sizeof(ifr));

   ifr.ifr_flags = flags;   /* IFF_TUN or IFF_TAP, plus maybe IFF_NO_PI */

   if (*dev) {
     /* if a device name was specified, put it in the structure; otherwise,
      * the kernel will try to allocate the "next" device of the
      * specified type */
     strncpy(ifr.ifr_name, dev, IFNAMSIZ);
   }

   /* try to create the device */
   if( (err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ) {
     close(fd);
     return err;
   }

  /* if the operation was successful, write back the name of the
   * interface to the variable "dev", so the caller can know
   * it. Note that the caller MUST reserve space in *dev (see calling
   * code below) */
  strcpy(dev, ifr.ifr_name);

  /* this is the special file descriptor that the caller will use to talk
   * with the virtual interface */
  return fd;
}


/* the bpf callout function.  it must be called with the mutex locked: */
static void
_tme_net_tap_callout(struct tme_net_tap *bpf, int new_callouts)
{
  struct tme_ethernet_connection *conn_eth;
  int callouts, later_callouts;
  unsigned int ctrl;
  int rc;
  int status;
  tme_ethernet_fid_t frame_id;
  struct tme_ethernet_frame_chunk frame_chunk_buffer;
  tme_uint8_t frame[TME_ETHERNET_FRAME_MAX];
  
  /* add in any new callouts: */
  bpf->tme_net_tap_callout_flags |= new_callouts;

  /* if this function is already running in another thread, simply
     return now.  the other thread will do our work: */
  if (bpf->tme_net_tap_callout_flags & TME_NET_TAP_CALLOUT_RUNNING) {
    return;
  }

  /* callouts are now running: */
  bpf->tme_net_tap_callout_flags |= TME_NET_TAP_CALLOUT_RUNNING;

  /* assume that we won't need any later callouts: */
  later_callouts = 0;

  /* loop while callouts are needed: */
  for (; (callouts = bpf->tme_net_tap_callout_flags) & TME_NET_TAP_CALLOUTS_MASK; ) {

    tme_log(&bpf->tme_net_tap_element->tme_element_log_handle, 1, TME_OK,
	    (&bpf->tme_net_tap_element->tme_element_log_handle,
	     _("callout %x"), callouts));
    /* clear the needed callouts: */
    bpf->tme_net_tap_callout_flags = callouts & ~TME_NET_TAP_CALLOUTS_MASK;
    callouts &= TME_NET_TAP_CALLOUTS_MASK;

    /* get our Ethernet connection: */
    conn_eth = bpf->tme_net_tap_eth_connection;

    /* if we need to call out new control information: */
    if (callouts & TME_NET_TAP_CALLOUT_CTRL) {

      /* form the new ctrl: */
      ctrl = 0;
      if (bpf->tme_net_tap_buffer_offset
	  < bpf->tme_net_tap_buffer_end) {
	ctrl |= TME_ETHERNET_CTRL_OK_READ;
      }

      /* unlock the mutex: */
      tme_mutex_unlock(&bpf->tme_net_tap_mutex);
      
      /* do the callout: */
      rc = (conn_eth != NULL
	    ? ((*conn_eth->tme_ethernet_connection_ctrl)
	       (conn_eth,
		ctrl))
	    : TME_OK);
	
      /* lock the mutex: */
      tme_mutex_lock(&bpf->tme_net_tap_mutex);
      
      /* if the callout was unsuccessful, remember that at some later
	 time this callout should be attempted again: */
      if (rc != TME_OK) {
	later_callouts |= TME_NET_TAP_CALLOUT_CTRL;
      }
    }
      
    /* if the Ethernet is readable: */
    if (callouts & TME_NET_TAP_CALLOUT_READ) {

      /* unlock the mutex: */
      tme_mutex_unlock(&bpf->tme_net_tap_mutex);
      
      /* make a frame chunk to receive this frame: */
      frame_chunk_buffer.tme_ethernet_frame_chunk_next = NULL;
      frame_chunk_buffer.tme_ethernet_frame_chunk_bytes = frame;
      frame_chunk_buffer.tme_ethernet_frame_chunk_bytes_count
	= sizeof(frame);

      /* do the callout: */
      rc = (conn_eth == NULL
	    ? TME_OK
	    : ((*conn_eth->tme_ethernet_connection_read)
	       (conn_eth,
		&frame_id,
		&frame_chunk_buffer,
		TME_ETHERNET_READ_NEXT)));
      
      /* lock the mutex: */
      tme_mutex_lock(&bpf->tme_net_tap_mutex);
      
      /* if the read was successful: */
      if (rc > 0) {

	/* check the size of the frame: */
	assert(rc <= sizeof(frame));

	/* do the write: */
	status = tme_thread_write(bpf->tme_net_tap_fd, frame, rc);

	/* writes must succeed: */
	assert (status == rc);

	/* mark that we need to loop to callout to read more frames: */
	bpf->tme_net_tap_callout_flags |= TME_NET_TAP_CALLOUT_READ;
      }

      /* otherwise, the read failed.  convention dictates that we
	 forget that the connection was readable, which we already
	 have done by clearing the CALLOUT_READ flag: */
    }

  }
  /* put in any later callouts, and clear that callouts are running: */
  bpf->tme_net_tap_callout_flags = later_callouts;
}

/* the BPF reader thread: */
static void
_tme_net_tap_th_reader(struct tme_net_tap *bpf)
{
  ssize_t buffer_end;
  unsigned long sleep_usec;
  
  /* lock the mutex: */
  tme_mutex_lock(&bpf->tme_net_tap_mutex);

  /* loop forever: */
  for (;;) {

    /* if the delay sleeping flag is set: */
    if (bpf->tme_net_tap_delay_sleeping) {

      /* clear the delay sleeping flag: */
      bpf->tme_net_tap_delay_sleeping = FALSE;
      
      /* call out that we can be read again: */
      _tme_net_tap_callout(bpf, TME_NET_TAP_CALLOUT_CTRL);
    }

    /* if a delay has been requested: */
    sleep_usec = bpf->tme_net_tap_delay_sleep;
    if (sleep_usec > 0) {

      /* clear the delay sleep time: */
      bpf->tme_net_tap_delay_sleep = 0;

      /* set the delay sleeping flag: */
      bpf->tme_net_tap_delay_sleeping = TRUE;

      /* unlock our mutex: */
      tme_mutex_unlock(&bpf->tme_net_tap_mutex);
      
      /* sleep for the delay sleep time: */
      tme_thread_sleep_yield(0, sleep_usec);
      
      /* lock our mutex: */
      tme_mutex_lock(&bpf->tme_net_tap_mutex);
      
      continue;
    }

    /* if the buffer is not empty, wait until either it is,
       or we're asked to do a delay: */
    if (bpf->tme_net_tap_buffer_offset
	< bpf->tme_net_tap_buffer_end) {
      tme_cond_wait_yield(&bpf->tme_net_tap_cond_reader,
			  &bpf->tme_net_tap_mutex);
      continue;
    }

    /* unlock the mutex: */
    tme_mutex_unlock(&bpf->tme_net_tap_mutex);

    /* read the BPF socket: */
    tme_log(&bpf->tme_net_tap_element->tme_element_log_handle, 1, TME_OK,
	    (&bpf->tme_net_tap_element->tme_element_log_handle,
	     _("calling read")));
    buffer_end = 
      tme_thread_read_yield(bpf->tme_net_tap_fd,
			    bpf->tme_net_tap_buffer,
			    bpf->tme_net_tap_buffer_size);

    /* lock the mutex: */
    tme_mutex_lock(&bpf->tme_net_tap_mutex);

    /* if the read failed: */
    if (buffer_end <= 0) {
      tme_log(&bpf->tme_net_tap_element->tme_element_log_handle, 1, errno,
	      (&bpf->tme_net_tap_element->tme_element_log_handle,
	       _("failed to read packets")));
      continue;
    }
    if (buffer_end < 64) buffer_end = 64;
    
    gettimeofday(&bpf->tme_net_tap_rx_tstamp, NULL);

    /* the read succeeded: */
    tme_log(&bpf->tme_net_tap_element->tme_element_log_handle, 1, TME_OK,
	    (&bpf->tme_net_tap_element->tme_element_log_handle,
	     _("read %ld bytes of packets"), (long) buffer_end));
    bpf->tme_net_tap_buffer_offset = 0;
    bpf->tme_net_tap_buffer_end = buffer_end;

    /* call out that we can be read again: */
    _tme_net_tap_callout(bpf, TME_NET_TAP_CALLOUT_CTRL);
  }
  /* NOTREACHED */
}

/* this is called when the ethernet configuration changes: */
static int
_tme_net_tap_config(struct tme_ethernet_connection *conn_eth, 
		    struct tme_ethernet_config *config)
{
  return (TME_OK);
}

/* this is called when control lines change: */
static int
_tme_net_tap_ctrl(struct tme_ethernet_connection *conn_eth, 
		  unsigned int ctrl)
{
  struct tme_net_tap *bpf;
  int new_callouts;

  /* recover our data structures: */
  bpf = conn_eth->tme_ethernet_connection.tme_connection_element->tme_element_private;

  /* assume that we won't need any new callouts: */
  new_callouts = 0;

  /* lock the mutex: */
  tme_mutex_lock(&bpf->tme_net_tap_mutex);

  /* if this connection is readable, call out a read: */
  if (ctrl & TME_ETHERNET_CTRL_OK_READ) {
    new_callouts |= TME_NET_TAP_CALLOUT_READ;
  }

  /* make any new callouts: */
  _tme_net_tap_callout(bpf, new_callouts);

  /* unlock the mutex: */
  tme_mutex_unlock(&bpf->tme_net_tap_mutex);

  return (TME_OK);
}

/* this is called to read a frame: */
static int
_tme_net_tap_read(struct tme_ethernet_connection *conn_eth, 
		  tme_ethernet_fid_t *_frame_id,
		  struct tme_ethernet_frame_chunk *frame_chunks,
		  unsigned int flags)
{
  struct tme_net_tap *bpf;
  struct tme_ethernet_frame_chunk frame_chunk_buffer;
  unsigned int count;
  int rc;

  /* recover our data structure: */
  bpf = conn_eth->tme_ethernet_connection.tme_connection_element->tme_element_private;

  /* lock our mutex: */
  tme_mutex_lock(&bpf->tme_net_tap_mutex);

  /* assume that we won't be able to return a packet: */
  rc = -ENOENT;

  /* loop until we have a good captured packet or until we 
     exhaust the buffer: */
  for (;;) {
    
    /* if this packet isn't big enough to even have an Ethernet header: */
    if (bpf->tme_net_tap_buffer_offset + sizeof(struct tme_ethernet_header)
	>= bpf->tme_net_tap_buffer_end) {
      if (bpf->tme_net_tap_buffer_offset != bpf->tme_net_tap_buffer_end) {
        tme_log(&bpf->tme_net_tap_element->tme_element_log_handle, 1, TME_OK,
	        (&bpf->tme_net_tap_element->tme_element_log_handle,
	         _("flushed short packet")));
        bpf->tme_net_tap_buffer_offset = bpf->tme_net_tap_buffer_end;
      }
      break;
    }

    /* if packets need to be delayed: */
    if (bpf->tme_net_tap_delay_time > 0) {
      
      /* if the current release time is before this packet's time: */
      if ((bpf->tme_net_tap_delay_release.tv_sec
	   < bpf->tme_net_tap_rx_tstamp.tv_sec)
	  || ((bpf->tme_net_tap_delay_release.tv_sec
	       == bpf->tme_net_tap_rx_tstamp.tv_sec)
	      && (bpf->tme_net_tap_delay_release.tv_usec
		  < bpf->tme_net_tap_rx_tstamp.tv_usec))) {

	/* update the current release time, by taking the current time
	   and subtracting the delay time: */
	gettimeofday(&bpf->tme_net_tap_delay_release, NULL);
	if (bpf->tme_net_tap_delay_release.tv_usec < bpf->tme_net_tap_delay_time) {
	  bpf->tme_net_tap_delay_release.tv_usec += 1000000UL;
	  bpf->tme_net_tap_delay_release.tv_sec--;
	}
	bpf->tme_net_tap_delay_release.tv_usec -= bpf->tme_net_tap_delay_time;
      }

      /* if the current release time is still before this packet's
         time: */
      if ((bpf->tme_net_tap_delay_release.tv_sec
	   < bpf->tme_net_tap_rx_tstamp.tv_sec)
	  || ((bpf->tme_net_tap_delay_release.tv_sec
	       == bpf->tme_net_tap_rx_tstamp.tv_sec)
	      && (bpf->tme_net_tap_delay_release.tv_usec
		  < bpf->tme_net_tap_rx_tstamp.tv_usec))) {

	/* set the sleep time: */
	assert ((bpf->tme_net_tap_delay_release.tv_sec
		 == bpf->tme_net_tap_rx_tstamp.tv_sec)
		|| ((bpf->tme_net_tap_delay_release.tv_sec + 1)
		    == bpf->tme_net_tap_rx_tstamp.tv_sec));
	bpf->tme_net_tap_delay_sleep
	  = (((bpf->tme_net_tap_delay_release.tv_sec
	       == bpf->tme_net_tap_rx_tstamp.tv_sec)
	      ? 0
	      : 1000000UL)
	     + bpf->tme_net_tap_rx_tstamp.tv_usec
	     - bpf->tme_net_tap_delay_release.tv_usec);

	/* rewind the buffer pointer: */
	bpf->tme_net_tap_buffer_offset = 0;

	/* stop now: */
	break;
      }
    }

    /* form the single frame chunk: */
    frame_chunk_buffer.tme_ethernet_frame_chunk_next = NULL;
    frame_chunk_buffer.tme_ethernet_frame_chunk_bytes
      = bpf->tme_net_tap_buffer;
    frame_chunk_buffer.tme_ethernet_frame_chunk_bytes_count
      = bpf->tme_net_tap_buffer_end;

    /* copy out the frame: */
    count = tme_ethernet_chunks_copy(frame_chunks, &frame_chunk_buffer);

    /* if this is a peek: */
    if (flags & TME_ETHERNET_READ_PEEK) {

      /* rewind the buffer pointer: */
      bpf->tme_net_tap_buffer_offset = 0;
    }

    /* otherwise, this isn't a peek: */
    else {

      /* update the buffer pointer: */
      bpf->tme_net_tap_buffer_offset = bpf->tme_net_tap_buffer_end;
    }

    /* success: */
    rc = count;
    break;
  }

  /* if the buffer is empty, or if we failed to read a packet,
     wake up the reader: */
  if ((bpf->tme_net_tap_buffer_offset
       >= bpf->tme_net_tap_buffer_end)
      || rc <= 0) {
    tme_cond_notify(&bpf->tme_net_tap_cond_reader, TRUE);
  }

  /* unlock our mutex: */
  tme_mutex_unlock(&bpf->tme_net_tap_mutex);
	
  /* done: */
  return (rc);
}

/* this makes a new Ethernet connection: */
static int
_tme_net_tap_connection_make(struct tme_connection *conn, unsigned int state)
{
  struct tme_net_tap *bpf;
  struct tme_ethernet_connection *conn_eth;
  struct tme_ethernet_connection *conn_eth_other;

  /* recover our data structures: */
  bpf = conn->tme_connection_element->tme_element_private;
  conn_eth = (struct tme_ethernet_connection *) conn;
  conn_eth_other = (struct tme_ethernet_connection *) conn->tme_connection_other;

  /* both sides must be Ethernet connections: */
  assert(conn->tme_connection_type == TME_CONNECTION_ETHERNET);
  assert(conn->tme_connection_other->tme_connection_type == TME_CONNECTION_ETHERNET);

  /* we're always set up to answer calls across the connection, so we
     only have to do work when the connection has gone full, namely
     taking the other side of the connection: */
  if (state == TME_CONNECTION_FULL) {

    /* lock our mutex: */
    tme_mutex_lock(&bpf->tme_net_tap_mutex);

    /* save our connection: */
    bpf->tme_net_tap_eth_connection = conn_eth_other;

    /* unlock our mutex: */
    tme_mutex_unlock(&bpf->tme_net_tap_mutex);
  }

  return (TME_OK);
}

/* this breaks a connection: */
static int
_tme_net_tap_connection_break(struct tme_connection *conn, unsigned int state)
{
  abort();
}

/* this makes a new connection side for a BPF: */
static int
_tme_net_tap_connections_new(struct tme_element *element, 
			     const char * const *args, 
			     struct tme_connection **_conns,
			     char **_output)
{
  struct tme_net_tap *bpf;
  struct tme_ethernet_connection *conn_eth;
  struct tme_connection *conn;

  /* recover our data structure: */
  bpf = (struct tme_net_tap *) element->tme_element_private;

  /* if we already have an Ethernet connection, do nothing: */
  if (bpf->tme_net_tap_eth_connection != NULL) {
    return (TME_OK);
  }

  /* allocate the new Ethernet connection: */
  conn_eth = tme_new0(struct tme_ethernet_connection, 1);
  conn = &conn_eth->tme_ethernet_connection;
  
  /* fill in the generic connection: */
  conn->tme_connection_next = *_conns;
  conn->tme_connection_type = TME_CONNECTION_ETHERNET;
  conn->tme_connection_score = tme_ethernet_connection_score;
  conn->tme_connection_make = _tme_net_tap_connection_make;
  conn->tme_connection_break = _tme_net_tap_connection_break;

  /* fill in the Ethernet connection: */
  conn_eth->tme_ethernet_connection_config = _tme_net_tap_config;
  conn_eth->tme_ethernet_connection_ctrl = _tme_net_tap_ctrl;
  conn_eth->tme_ethernet_connection_read = _tme_net_tap_read;

  /* return the connection side possibility: */
  *_conns = conn;

  /* done: */
  return (TME_OK);
}

/* the new BPF function: */
TME_ELEMENT_SUB_NEW_DECL(tme_host_net,tap) {
  struct tme_net_tap *bpf;
  int tap_fd;
  int saved_errno;
  u_int packet_buffer_size = 1536;
  char tap_name[IFNAMSIZ];
  unsigned long delay_time;
  int arg_i;
  int usage;
  
  /* check our arguments: */
  usage = 0;
  tap_name[0] = 0;
  delay_time = 0;
  arg_i = 1;
  for (;;) {

    /* the interface we're supposed to use: */
    if (TME_ARG_IS(args[arg_i + 0], "interface")
	&& args[arg_i + 1] != NULL) {
      strncpy(tap_name, args[arg_i + 1], sizeof tap_name -1);
      arg_i += 2;
    }

    /* a delay time in microseconds: */
    else if (TME_ARG_IS(args[arg_i + 0], "delay")
	     && (delay_time = tme_misc_unumber_parse(args[arg_i + 1], 0)) > 0) {
      arg_i += 2;
    }
    
    /* if we ran out of arguments: */
    else if (args[arg_i + 0] == NULL) {
      break;
    }

    /* otherwise this is a bad argument: */
    else {
      tme_output_append_error(_output,
			      "%s %s", 
			      args[arg_i],
			      _("unexpected"));
      usage = TRUE;
      break;
    }
  }

  if (usage) {
    tme_output_append_error(_output,
			    "%s %s [ interface %s ] [ delay %s ]",
			    _("usage:"),
			    args[0],
			    _("INTERFACE"),
			    _("MICROSECONDS"));
    return (EINVAL);
  }

  if ((tap_fd = _tme_net_tap_alloc(tap_name, IFF_TAP | IFF_NO_PI)) < 0) {
    /* we failed to open this device. */
    saved_errno = errno;
    tme_log(&element->tme_element_log_handle, 1, saved_errno,
	    (&element->tme_element_log_handle, 
	     "%s", tap_name));

    return (saved_errno);
  }
  tme_log(&element->tme_element_log_handle, 1, TME_OK,
	  (&element->tme_element_log_handle,
	   "opened %s",
	   tap_name));

  /* start our data structure: */
  bpf = tme_new0(struct tme_net_tap, 1);
  bpf->tme_net_tap_element = element;
  bpf->tme_net_tap_fd = tap_fd;
  bpf->tme_net_tap_buffer_size = packet_buffer_size;
  bpf->tme_net_tap_buffer = tme_new(tme_uint8_t, packet_buffer_size);
  bpf->tme_net_tap_delay_time = delay_time;

  /* start the threads: */
  tme_mutex_init(&bpf->tme_net_tap_mutex);
  tme_cond_init(&bpf->tme_net_tap_cond_reader);
  tme_thread_create((tme_thread_t) _tme_net_tap_th_reader, bpf);

  /* fill the element: */
  element->tme_element_private = bpf;
  element->tme_element_connections_new = _tme_net_tap_connections_new;

  return (TME_OK);
}
